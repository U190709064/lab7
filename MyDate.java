public class MyDate {

    private int day, month, year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    int[] how_many_days = {31,28,31,30,31,30,31,30,31,30,31,30};

    private boolean leapYear() {
        return year % 4 == 0;
    }

    public int getMonth() {
        return month++;
    }

    public void incrementDay() {
        day+=1;
        if (day >how_many_days[month]) {
            if (month == 1 && leapYear()) {
                day = 29;
            }else {
                day = 1;
                incrementMonth();
            }
        }
    }

    public void decrementDay() {
        day-=1;
        if (day == 0); {
            decrementMonth();
            if (month == 1 && leapYear()) {
                day = 29;
            }else {
                day = how_many_days[month];
            }
        }
    }

    public void decrementMonth() {
        decrementMonth(-1);
    }

    public void incrementYear(int i) {
        year+=i;
        if (month ==1 && day ==29 && !leapYear()){
            day = 28;
        }
    }

    public void  incrementDay(int i) {
        while (i > 0){
            incrementDay();
            i--;
        }
    }

    public void decrementDay(int i) {
        while (i > 0){
            decrementDay();
            i--;
        }
    }

    public void incrementMonth(int i) {
        int newMonth = (month + i) % 12;
        int yearDifference = 0;
        if (newMonth < 0) {
            newMonth += 12;
            yearDifference = -1;
        }
        yearDifference = (month + i) / 12;
        month = newMonth;
        incrementYear(yearDifference);
    }

    public void decrementMonth(int i) {
        incrementMonth(-i);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void decrementYear() {
        decrementYear(-1);
    }

    public void decrementYear(int i) {
        incrementYear(-i);
    }

    public void incrementYear() {
        incrementYear(-1);
    }

    public boolean isBefore(MyDate anotherDate) {
        int a = Integer.parseInt(toString().replaceAll("-", "")) ;

        int b = Integer.parseInt(anotherDate.toString().replaceAll("-", ""));

        return a < b;
    }

    public boolean isAfter(MyDate anotherDate) {
        int a = Integer.parseInt(toString().replaceAll("-", "")) ;
        int b = Integer.parseInt(anotherDate.toString().replaceAll("-", ""));
        return a > b;
    }

    public int dayDifference(MyDate anotherDate) {
        int dayDifference = 0;
        if (isBefore(anotherDate)){
            MyDate date = new MyDate(day,month+1,year);
            while (date.isBefore(anotherDate)){
                date.incrementDay();
                dayDifference+=1;
            }
        }else if(isAfter(anotherDate)){
            MyDate date = new MyDate(day,month+1,year);
            while (date.isAfter(anotherDate)){
                date.decrementDay();
                dayDifference+=1;
            }

        }
        return dayDifference;
    }

    public String toString(){
        return year + "-"+(month + 1 < 10 ? "0" : "") + (month + 1) + "-" +(day < 10 ? "0" : "") + day;
    }

}